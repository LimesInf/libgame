package com.mygdx.game.levels;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Disposable;
import com.mygdx.game.Constants;
import com.mygdx.game.NGame;
import com.mygdx.game.controllers.Handler;
import com.mygdx.game.controllers.MapHandler;
import com.mygdx.game.physThings.MapPhysObject;
import com.mygdx.game.physThings.PhysBody;

import java.util.ArrayList;

public abstract class AbstractLevel implements Disposable {

    public static ArrayList<PhysBody> physBodies = new ArrayList<>();
    public static ArrayList<Handler> handlers = new ArrayList<>();

    public static final TmxMapLoader mapLoader = new TmxMapLoader();
    public MapHandler mapHandler;
    protected TiledMapRenderer renderer;
    public TiledMap map;

    private NGame game;

    public AbstractLevel(NGame game) {
        mapHandler = MapHandler.getInstance();
        this.game = game;

    }

    public void init() {
        initMapBodies();
        for (PhysBody physBody : physBodies) {
            physBody.initPhysics();
        }
    }

    public void render(float dt) {
        for (Handler handler : handlers) {
            handler.handle();
        }
    }

    @Override
    public void dispose() {
        map.dispose();
    }

    private void initMapBodies() {
        ArrayList<String> keys = new ArrayList<>();
        keys.add(Constants.MAP_DEFAULTS.SIDE_OBJECT.getValue());
        keys.add(Constants.MAP_DEFAULTS.ALTERNATIV_SIDE_OBJECTS.getValue());
        for (String key : keys) {
            for (MapObject mapObject : map.getLayers().get(key).getObjects().getByType(RectangleMapObject.class)) {
                Rectangle rect = ((RectangleMapObject) mapObject).getRectangle();
                float x = (rect.getX() + rect.getWidth() / 2) / Constants.SCALE_FACTOR;
                float y = (rect.getY() + rect.getHeight() / 2) / Constants.SCALE_FACTOR;
                float shapeWidth = (rect.getWidth() / 2) / Constants.SCALE_FACTOR;
                float shapeHeight = (rect.getHeight() / 2) / Constants.SCALE_FACTOR;
                MapPhysObject.Builder builder = MapPhysObject.newBuilder();
                builder.world(game.getWorld()).x(x).y(y).shapeHeight(shapeHeight).shapeWidth(shapeWidth);
                physBodies.add(builder.build());
            }
        }
    }
}
