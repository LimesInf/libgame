package com.mygdx.game.levels;

import com.badlogic.gdx.maps.tiled.renderers.OrthoCachedTiledMapRenderer;
import com.mygdx.game.Constants;
import com.mygdx.game.NGame;
import com.mygdx.game.physThings.Player;

public class FirstLevel extends AbstractLevel  {
    private NGame game;
    private Player player;
    public FirstLevel(NGame game) {
        super(game);
        this.game = game;
        map = mapLoader.load("level/untitled.tmx");
        renderer = new OrthoCachedTiledMapRenderer(map , 1 / Constants.SCALE_FACTOR);
        physBodies.add(game.getPlayer());
        handlers.add(game.getPlayer());
        mapHandler.setLevel(this);
        init();

    }
    @Override
    public void render(float dt) {
        super.render(dt);
        renderer.setView(game.getCamera());
        renderer.render();
        mapHandler.handle();
    }

}
