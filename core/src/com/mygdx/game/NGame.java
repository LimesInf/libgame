package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.FitViewport;


import com.mygdx.game.physThings.Player;
import com.mygdx.game.levels.FirstLevel;
import lombok.Getter;

public class NGame extends Game {
    @Getter
    private FPSLogger logger;
    @Getter
    private OrthographicCamera camera;
    @Getter
    private FitViewport viewPort;
    @Getter
    private Player player;

    private FirstLevel firstLevel;
    @Getter
    private World world;
    Box2DDebugRenderer dDebugRenderer;



    @Override
    public void create() {
        viewPort = new FitViewport(Constants.VIEWPORT_W / Constants.SCALE_FACTOR,Constants.VIEWPORT_H / Constants.SCALE_FACTOR);
        camera = new OrthographicCamera();
        camera.setToOrtho(false,viewPort.getWorldWidth() ,viewPort.getWorldHeight());
        camera.update();
        dDebugRenderer = new Box2DDebugRenderer();
        world = new World(new Vector2(0,-9.8f),false);
        player = new Player(this);
        firstLevel = new FirstLevel(this);
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        world.step(1/60f,5,2);
        firstLevel.render(Gdx.graphics.getDeltaTime());
        dDebugRenderer.render(world,camera.combined);

    }

    @Override
    public void dispose() {
        dDebugRenderer.dispose();
    }
}
