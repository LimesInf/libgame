package com.mygdx.game.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.maps.MapLayers;
import com.mygdx.game.Constants;
import com.mygdx.game.GameException;
import com.mygdx.game.levels.AbstractLevel;

public class MapHandler implements Handler {


    private AbstractLevel level;
    private MapLayers layers;

    private static MapHandler instance;

    private MapHandler() {
    }


    public static MapHandler getInstance()  {
        if (instance == null) {
            instance = new MapHandler();
        }
        return instance;
    }

    @Override
    public void handle() {
        if (Gdx.input.isKeyPressed(Input.Keys.R)) {
            layers.get(Constants.MAP_DEFAULTS.LAYER_1.getValue()).setVisible(true);
            layers.get(Constants.MAP_DEFAULTS.LAYER_2.getValue()).setVisible(false);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.E)) {
            layers.get(Constants.MAP_DEFAULTS.LAYER_1.getValue()).setVisible(false);
            layers.get(Constants.MAP_DEFAULTS.LAYER_2.getValue()).setVisible(true);
        }
    }

    public void setLevel(AbstractLevel level) {
        this.level = level;
        layers = level.map.getLayers();

    }
}
