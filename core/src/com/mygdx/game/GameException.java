package com.mygdx.game;

public class GameException extends Exception {
    public GameException(String desc) {
        super(desc);
    }
}
