package com.mygdx.game.physThings;

import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Disposable;
import lombok.Getter;

@Getter
public abstract class DefaultPhysThing  implements Disposable {
    protected BodyDef bodyDef;
    protected Body body;
    protected Fixture fixture;
    protected FixtureDef fixtureDef;
    protected Shape shape;

    abstract void render();

    @Override
    public void dispose() {
        shape.dispose();
    }
}
