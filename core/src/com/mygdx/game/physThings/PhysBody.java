package com.mygdx.game.physThings;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;

public interface PhysBody {

    void  initBody();
    void initFixture();
    void initBodyDef();
    void initFixtureDef();
    default void initPhysics() {

        initBodyDef();
        initBody();
        initFixtureDef();
        initFixture();
        System.out.println("done");
    }
}
