package com.mygdx.game.physThings;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.Constants;
import com.mygdx.game.NGame;
import com.mygdx.game.controllers.Handler;
import com.mygdx.game.physThings.PhysBody;
import jdk.nashorn.internal.objects.annotations.Getter;


public class Player extends DefaultPhysThing implements Handler, PhysBody {

    private Camera camera;

    private World world;

    public Player(NGame game) {
        this.camera = game.getCamera();
        this.world = game.getWorld();
    }

    @Override
    public void handle() {
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT) && body.getLinearVelocity().x <= 0.4f) {
            body.applyLinearImpulse(new Vector2(0.03f, 0), body.getWorldCenter(), false);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT) && body.getLinearVelocity().x >= -0.4f) {
            body.applyLinearImpulse(new Vector2(-0.03f, 0), body.getWorldCenter(), false);
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.UP) && body.getLinearVelocity().y <= 0.3f     ) {
            body.applyLinearImpulse(new Vector2(0f, 0.03f), body.getWorldCenter(), false);
        } else{
            body.getLinearVelocity().y = 0;
            body.getLinearVelocity().x = 0;
        }
        System.out.println(body.getLinearVelocity().x);
        camera.position.x = body.getPosition().x;
        camera.update();
    }

    @Override
    public void initBody() {
        body = world.createBody(bodyDef);

    }

    @Override
    public void initFixture() {
        body.createFixture(fixtureDef);
    }

    @Override
    public void initBodyDef() {
        bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(new Vector2(32 / Constants.SCALE_FACTOR, 32 / Constants.SCALE_FACTOR));
    }

    @Override
    public void initFixtureDef() {
        shape = new CircleShape();
        shape.setRadius(5f / Constants.SCALE_FACTOR);
        fixtureDef = new FixtureDef();
        fixtureDef.density = 0.3f;
        fixtureDef.shape = shape;
    }

    @Override
    public void render() {

    }
}
