package com.mygdx.game.physThings;

import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class MapPhysObject extends DefaultPhysThing implements PhysBody {

    private World world;
    private float x;
    private float y;
    private float shapeWidth;
    private float shapeHeight;


    private MapPhysObject(Builder builder) {
        this.world = builder.world;
        this.x = builder.x;
        this.y = builder.y;
        this.shapeWidth = builder.shapeWidth;
        this.shapeHeight = builder.shapeHeight;

    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static class Builder {
        private float x;
        private float y;
        private float shapeWidth;
        private float shapeHeight;
        private World world;

        Builder() {

        }

        public Builder x(float x) {
            this.x = x;
            return this;
        }

        public Builder y(float y) {
            this.y = y;
            return this;
        }

        public Builder world(World world) {
            this.world = world;
            return this;
        }


        public Builder shapeWidth(float shapeWidth) {
            this.shapeWidth = shapeWidth;
            return this;
        }

        public Builder shapeHeight(float shapeHeight) {
            this.shapeHeight = shapeHeight;
            return this;
        }

        public MapPhysObject build() {
            return new MapPhysObject(this);
        }
    }

    @Override
    void render() {

    }

    @Override
    public void initBody() {
        body = world.createBody(bodyDef);
    }

    @Override
    public void initFixture() {
        body.createFixture(fixtureDef);
    }

    @Override
    public void initBodyDef() {
        bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set(x, y);
    }

    @Override
    public void initFixtureDef() {
        shape = new PolygonShape();
        fixtureDef = new FixtureDef();
        ((PolygonShape) shape).setAsBox(shapeWidth, shapeHeight);
        fixtureDef.shape = shape;
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
