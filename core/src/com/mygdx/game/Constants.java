package com.mygdx.game;

import lombok.Getter;

public final class Constants {
    public static final int VIEWPORT_W = 400;
    public static final int VIEWPORT_H = 208;

    public static final float SCALE_FACTOR = 50;

    public enum MAP_DEFAULTS {
        LAYER_1("side"),
        LAYER_2("alternativSide"),
        SIDE_OBJECT("sideObjects"),
        ALTERNATIV_SIDE_OBJECTS("alternativSideObjects");
        @Getter
        private String value;
        MAP_DEFAULTS(String value) {
            this.value = value;
        }

    }
}
